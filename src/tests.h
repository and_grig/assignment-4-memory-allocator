#pragma once

#include <stdbool.h>

bool usual_allocation_test();
bool free_one_block_test();
bool free_two_blocks_test();
bool new_region_extends_old_test();
bool new_region_in_another_place_test();
void run_tests();
