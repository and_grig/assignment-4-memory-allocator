#define _DEFAULT_SOURCE

#include "tests.h"
#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <stdio.h>
#include <stdlib.h>

static inline size_t get_size(size_t size){
    return size_max(size_from_capacity((block_capacity){size}).bytes, REGION_MIN_SIZE);
}

bool usual_allocation_test(){
printf("Test 1: Обычное успешное выделение памяти");
    void* heap = heap_init(REGION_MIN_SIZE);
    if (!heap) return false;

    void* allocated = _malloc(REGION_MIN_SIZE/2);
    if (!allocated) {
        munmap(heap, get_size(REGION_MIN_SIZE));
        return false;
    }

    debug_heap(stdout, heap);
    _free(allocated);
    munmap(heap, get_size(REGION_MIN_SIZE));

    return true;
}

bool free_one_block_test(){
    printf("Test 2: Освобождение одного блока из нескольких выделенных\n");
    void* heap = heap_init(REGION_MIN_SIZE);
    if (!heap) return false;

    size_t size = 4;
    void* allocated[size];
    for (size_t i = 0; i < size; i++){
        allocated[i] = _malloc(REGION_MIN_SIZE/size/2);
        if (!allocated[i]) {
            munmap(heap, get_size(REGION_MIN_SIZE));
            return false;
        }
    }

    _free(allocated[0]);
    _free(allocated[2]);

    debug_heap(stdout, heap);

    munmap(heap, get_size(REGION_MIN_SIZE));

    return true;
}

bool free_two_blocks_test(){
    printf("Test 3: Освобождение двух блоков из нескольких выделенных\n");
    void* heap = heap_init(REGION_MIN_SIZE);
    if (!heap) return false;

    size_t size = 4;
    void* allocated[size];
    for (size_t i = 0; i < size; i++){
        allocated[i] = _malloc(REGION_MIN_SIZE/size/2);
        if (!allocated[i]) {
            munmap(heap, get_size(REGION_MIN_SIZE));
            return false;
        }
    }

    _free(allocated[0]);
    _free(allocated[1]);

    debug_heap(stdout, heap);

    munmap(heap, get_size(REGION_MIN_SIZE));

    return true;
}

bool new_region_extends_old_test(){
    printf("Test 4: Память закончилась, новый регион памяти расширяет старый\n");
    void* heap = heap_init(REGION_MIN_SIZE);
    if (!heap) return false;

    void* allocated = _malloc(REGION_MIN_SIZE*2);
    if (!allocated) {
        munmap(heap, get_size(REGION_MIN_SIZE));
        return false;
    }

    debug_heap(stdout, heap);
    munmap(heap, get_size(REGION_MIN_SIZE*2));

    return true;
}

bool new_region_in_another_place_test(){
    size_t heap_size = REGION_MIN_SIZE;
    size_t free_size = REGION_MIN_SIZE/2;
    printf("Test 5: Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.\n");
    void* heap = heap_init(heap_size);
    if (!heap) return false;


    void* space = mmap(heap + heap_size*2, free_size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);
    if (space == MAP_FAILED) {
        printf("mmap failed\n");
        munmap(heap, heap_size);
        return false;
    }
    if (space != heap + heap_size*2) {
        printf("mmap returned wrong address\n");
        munmap(heap, heap_size);
        return false;
    }

    void* allocated = _malloc(heap_size*3);
    debug_heap(stdout, heap);

    if (!allocated) {
        printf("malloc failed\n");
        munmap(heap, heap_size);
        return false;
    }

    if (allocated < heap + heap_size*3) {
        printf("malloc returned wrong address\n");
        munmap(heap, heap_size);
        return false;
    }

    munmap(heap, heap_size);

    return true;
}



void run_tests(){
    if (!usual_allocation_test()) {
        printf("Test 1: failed");
    }
    if (!free_one_block_test()) {
        printf("Test 2: failed");
    }
    if (!free_two_blocks_test()) {
        printf("Test 3: failed");
    }
    if (!new_region_extends_old_test()) {
        printf("Test 4: failed");
    }
    if (!new_region_in_another_place_test()) {
        printf("Test 5: failed");
    }
}